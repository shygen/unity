﻿#region Disclaimer
// <copyright file="GenerationCustomEditor.cs">
// Copyright (c) 2017 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion
namespace Shygen.Unity.Editor.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class)]
    public class GenerationCustomEditorAttribute : Attribute
    {
        public Type CategoryType { get; private set; }
        
        public GenerationCustomEditorAttribute(Type categoryType)
        {
            CategoryType = categoryType;
        }
    }
}