﻿#region Disclaimer

// <copyright file="UnityCodeGenerationHelper.cs">
// Copyright (c) 2017 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace Shygen.Unity.Editor
{
    using System.Collections.Generic;
    using System.IO;
    using Newtonsoft.Json;
    using RobinBird.Logging.Runtime;
    using RobinBird.Persistence.Json;
    using RobinBird.Persistence.Runtime;
    using RobinBird.Utilities.Unity.Editor.Helper;
    using Shygen.Editor;
    using UnityEditor;
    using UnityEngine;
    using JsonSerializer = Newtonsoft.Json.JsonSerializer;

    public static class UnityCodeGenerationHelper
    {
        private const string SettingsName = "CodeGenerationSettings";

        private static IPersistenceProvider _moduleSerializer;
        private static IPersistenceProvider _settingsSerializer;
        private static string _editorSettingsPath;

        [InitializeOnLoadMethod]
        public static void InitializeSettings()
        {
            CodeGenerationHelper.generationEditorSettings = CodeGenerationEditorSettings.Instance;
        }

        public static IPersistenceProvider ModuleSerializer
        {
            get
            {
                if (_moduleSerializer == null)
                {
                    var settings = new JsonSerializerSettings();
                    settings.TypeNameHandling = TypeNameHandling.Auto;
                    settings.Formatting = Formatting.Indented;
                    JsonSerializer serializer = JsonSerializer.Create(settings);
                    _moduleSerializer = new JsonPersistenceProvider(serializer);
                }
                return _moduleSerializer;
            }
        }

        private static IPersistenceProvider SettingsSerializer
        {
            get
            {
                if (_settingsSerializer == null)
                {
                    var settings = new JsonSerializerSettings
                    {
                        Formatting = Formatting.Indented
                    };
                    _settingsSerializer = new JsonPersistenceProvider(JsonSerializer.Create(settings));
                }
                return _settingsSerializer;
            }
        }

        private static string EditorSettingsPath
        {
            get
            {
                if (_editorSettingsPath == null)
                {
                    string projectSettings = EditorTools.GetProjectSettingsPath();
                    _editorSettingsPath = Path.Combine(projectSettings, SettingsName);
                }
                return _editorSettingsPath;
            }
        }

        public static void WriteClassModuleFile(TextAsset moduleFile, Module module)
        {
            if (moduleFile == null || module == null)
            {
                return;
            }

            string classModulePath = AssetDatabase.GetAssetPath(moduleFile);
            string filePath = Path.Combine(EditorTools.GetProjectPath(), classModulePath);

            Persistence.Serialize<Module>(ModuleSerializer, filePath, module);
            AssetDatabase.Refresh();
        }

        public static Module LoadClassModuleFile(TextAsset moduleFile)
        {
            if (moduleFile == null)
            {
                return null;
            }

            string classModulePath = AssetDatabase.GetAssetPath(moduleFile);
            string filePath = Path.Combine(EditorTools.GetProjectPath(), classModulePath);

            return Persistence.Deserialize<Module>(ModuleSerializer, filePath);
        }


        public static TextAsset CreateNewModuleFile()
        {
            string savePath = EditorUtility.SaveFilePanel("Save...", Application.dataPath, string.Format("moduleFile.{0}", ModuleSerializer.Extension),
                ModuleSerializer.Extension);

            var newModule = new Module();
            newModule.Name = Path.GetFileNameWithoutExtension(savePath);
            Persistence.Serialize<Module>(ModuleSerializer, savePath, newModule);


            string relativePath = savePath.Substring(EditorTools.GetProjectPath().Length + 1);

            AssetDatabase.ImportAsset(relativePath, ImportAssetOptions.ForceUpdate);
            var asset = AssetDatabase.LoadAssetAtPath<TextAsset>(relativePath);

            AssetDatabase.Refresh();

            return asset;
        }

        public static CodeGenerationSettings ReadEditorSettings()
        {
            string settingsPath = SettingsSerializer.GetPathWithExtension(EditorSettingsPath);
            if (File.Exists(settingsPath) == false)
            {
                // There is no UserSettings Object
                Log.WarnFormat("Could not retrieve {1} Object at Path: '{0}'. Creating new one.",new object[]{ settingsPath, SettingsName});
                var codeGenerationSettings = new CodeGenerationSettings();
                codeGenerationSettings.CategorySettings = new List<CodeGenerationSettings.CodeGenerationCategorySetting>();
                codeGenerationSettings.ModuleGroupSettings = new List<CodeGenerationSettings.ModuleGroupSetting>();
                return codeGenerationSettings;
            }

            return Persistence.Deserialize<CodeGenerationSettings>(SettingsSerializer, EditorSettingsPath);
        }

        public static void WriteEditorSettings(CodeGenerationSettings settings)
        {
            Persistence.Serialize<CodeGenerationSettings>(SettingsSerializer, EditorSettingsPath, settings);
        }

        public static void GenerateFiles(CodeGenerationSettings settings, string modulePath, CodeGenerationSettings.ModuleGroupSetting group)
        {
            if (string.IsNullOrEmpty(modulePath))
            {
                Log.Error("Can not generate files with empty module path.");
                return;
            }

            var moduleText = AssetDatabase.LoadAssetAtPath<TextAsset>(modulePath);

            if (moduleText == null)
            {
                Log.ErrorFormat("Could not find module at path: {0}",new object[]{ modulePath});
                return;
            }
            Module module = LoadClassModuleFile(moduleText);
            CodeGenerationHelper.GenerateFiles(EditorTools.GetProjectPath(), module, settings, group);
        }
    }
}