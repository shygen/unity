﻿namespace Shygen.Unity.Editor.EditorWindows
{
    using System;
    using System.Collections.Generic;
    using Attributes;
    using Interfaces;
    using JetBrains.Annotations;
    using RobinBird.Logging.Runtime;
    using RobinBird.Utilities.Runtime.Extensions;
    using RobinBird.Utilities.Unity.Editor.Helper;
    using Shygen.Editor;
    using Shygen.Editor.Interfaces;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// Editor window to configure a module.
    /// Configure entries in every <see cref="ICodeGenerationCategory" /> available.
    /// </summary>
    public class ModuleEditorWindow : EditorWindow
    {
        private const float SeparatorWidth = 9;
        private const float TabBarHeight = 40;
        private const float TabBarButtonWidth = 100;
        private const float ContentToTabBarDistance = 20;
        private const float EdgePadding = 6;
        private const string NoModuleLoadedString = "<No module loaded>";


        private readonly Dictionary<Type, IGenerationCategoryEditor> editorCache = new Dictionary<Type, IGenerationCategoryEditor>();

        private float hierarchyWidth;
        private string moduleName = NoModuleLoadedString;

        private Vector2 hierarchyScrollPosition;
        private Vector2 inspectorScrollPosition;

        private GUIStyle boxStyle;
        private GUIStyle selectedLabelStyle;

        [NonSerialized]
        private TextAsset moduleFile;

        private TextAsset pendingModuleFile;

        [NonSerialized]
        private Module module;

        private DataDraft selectedDataDraft;

        [NonSerialized]
        private DataDraft pendingDataDraftSelection;

        private ICodeGenerationCategory category;
        private ICodeGenerationCategory pendingCategory;
        private IGenerationCategoryEditor categoryEditor;

        private GuiSwitch createNewModuleFileSwitch = default(GuiSwitch);
        private GuiSwitch saveModuleFileSwitch = default(GuiSwitch);
        private GuiSwitch addDataSwitch = default(GuiSwitch);
        private GuiSwitch delDataSwitch = default(GuiSwitch);
        private GuiSwitch nameModifiedSwitch = default(GuiSwitch);
        private GuiSwitch openConfigSwitch = default(GuiSwitch);
        private GuiSwitch generateFilesSwitch = default(GuiSwitch);

        private List<DataDraft> DataDrafts
        {
            get
            {
                if (module == null)
                {
                    return null;
                }
                if (module.DataDrafts == null)
                {
                    module.DataDrafts = new List<DataDraft>();
                }
                return module.DataDrafts;
            }
        }

        public GUIStyle SelectedLabelStyle
        {
            get
            {
                if (selectedLabelStyle == null)
                {
                    selectedLabelStyle = new GUIStyle(GUI.skin.label);
                    selectedLabelStyle.normal.background = EditorTools.MakeTexture(1, 1, new Color(0.6784314f, 0.8470588f, 0.9019608f));
                }
                return selectedLabelStyle;
            }
        }

        [UsedImplicitly]
        private void OnEnable()
        {
            // Set default Values
            boxStyle = new GUIStyle();
            boxStyle.normal.background = EditorGUIUtility.whiteTexture;

            // Initializing Categories
            if (CodeGenerationHelper.GetCodeGenerationCategories().Length > 0)
            {
                pendingCategory = CodeGenerationHelper.GetCodeGenerationCategories()[0];
            }

            // Setting initial data selection
            SetInitialData();
        }

        private void SetInitialData()
        {
            if (module == null || category == null)
            {
                return;
            }

            DataDraft[] datas = CodeGenerationHelper.GetDataDraftsForCategory(category, module);

            if (datas != null && datas.Length > 0)
            {
                pendingDataDraftSelection = datas[0];
            }
            else
            {
                pendingDataDraftSelection = null;
            }
        }

        [UsedImplicitly]
        private void Update()
        {
            if (saveModuleFileSwitch.Use())
            {
                UnityCodeGenerationHelper.WriteClassModuleFile(moduleFile, module);
            }

            if (createNewModuleFileSwitch.Use())
            {
                // Create a new File
                pendingModuleFile = UnityCodeGenerationHelper.CreateNewModuleFile();
            }

            if (openConfigSwitch.Use())
            {
                CodeGenerationSettingsEditorWindow.GetWindow();
            }

            if (generateFilesSwitch.Use())
            {
                var codeGenerationSettings = UnityCodeGenerationHelper.ReadEditorSettings();
                CodeGenerationSettings.ModuleGroupSetting group = codeGenerationSettings.GetModuleGroup(AssetDatabase.GetAssetPath(moduleFile));

                if (group != null)
                {
                    CodeGenerationHelper.GenerateFiles(EditorTools.GetProjectPath(), module, codeGenerationSettings, group);
                    AssetDatabase.Refresh();
                }
                else
                {
                    Log.Warn("Can not generate code for this module because it is not added to a generation group. Please add it by pressing on 'Config' button.");
                }
            }

            if (moduleFile != pendingModuleFile)
            {
                // Class Module File changed
                moduleFile = pendingModuleFile;

                module = UnityCodeGenerationHelper.LoadClassModuleFile(moduleFile);

                // Update Name for name field
                moduleName = module != null ? module.Name : NoModuleLoadedString;

                SetInitialData();
            }

            if (category != pendingCategory)
            {
                category = pendingCategory;

                if (category != null)
                {
                    categoryEditor = GetCategoryEditor();
                    if (categoryEditor != null)
                    {
                        categoryEditor.Initialize(category);
                    }
                }
                else
                {
                    categoryEditor = null;
                }
                // Update Values
                SetInitialData();
            }

            if (category == null)
            {
                return;
            }

            if (selectedDataDraft != pendingDataDraftSelection)
            {
                // Selection changed
                selectedDataDraft = pendingDataDraftSelection;

                if (categoryEditor != null)
                {
                    categoryEditor.SelectedDataDraft = selectedDataDraft;
                }

                Repaint();
            }

            if (addDataSwitch.Use())
            {
                DataDraft dataDraft = CodeGenerationHelper.CreateDataDraft(category.GetType());
                dataDraft.Name = CodeGenerationHelper.GetUniquePropertyName("DataEntry", DataDrafts, draft => draft.Name);

                DataDrafts.Add(dataDraft);


                // If no data was selected. Select some.
                if (selectedDataDraft == null)
                {
                    SetInitialData();
                }

                Repaint();
            }

            if (delDataSwitch.Use())
            {
                if (selectedDataDraft != null)
                {
                    DataDrafts.Remove(selectedDataDraft);

                    SetInitialData();
                }
                Repaint();
            }

            if (nameModifiedSwitch.Use())
            {
                if (module != null)
                {
                    module.Name = moduleName;
                }
            }
        }

        [CanBeNull]
        private IGenerationCategoryEditor GetCategoryEditor()
        {
            IGenerationCategoryEditor editor;
            var categoryType = category.GetType();
            if (editorCache.TryGetValue(categoryType, out editor) == false)
            {
                var categoryEditors = AppDomain.CurrentDomain.GetTypes(typeof(IGenerationCategoryEditor));
                for (int i = 0; i < categoryEditors.Length; i++)
                {
                    var categroyEditorType = categoryEditors[i];
                    var categoryAttribute = categroyEditorType.GetCustomAttribute<GenerationCustomEditorAttribute>(false);
                    if (categoryAttribute == null)
                    {
                        continue;
                    }
                    if (categoryType == categoryAttribute.CategoryType)
                    {
                        var newEditorInstance = (IGenerationCategoryEditor)Activator.CreateInstance(categroyEditorType);
                        editor = newEditorInstance;
                        editorCache.Add(categoryType, newEditorInstance);
                    }
                }
            }
            return editor;
        }

        [UsedImplicitly]
        private void OnGUI()
        {
            if (Event.current.type == EventType.Layout)
            {
                hierarchyWidth = Screen.width * 0.33f - 2 * EdgePadding - SeparatorWidth;
            }

            var screenRect = new Rect(0 + EdgePadding, 0 + EdgePadding, position.width - EdgePadding, position.height - EdgePadding);

            float movingX = screenRect.x;
            float movingY = screenRect.y;


            float toolbarHeight = EditorGUIUtility.singleLineHeight;
            var toolbarRect = new Rect(movingX, movingY, screenRect.width, toolbarHeight);
            DrawToolBar(toolbarRect);

            movingY += toolbarHeight + EdgePadding;

            if (module == null)
            {
                const float helpBoxWidth = 200;
                const float helpBoxHeight = 100;
                var helpBoxRect = new Rect(screenRect.center.x - helpBoxWidth * 0.5f, movingY, helpBoxWidth, helpBoxHeight);
                EditorGUI.HelpBox(helpBoxRect, "No Data", MessageType.Warning);
                return;
            }

            if (category == null)
            {
                const float helpBoxWidth = 200;
                const float helpBoxHeight = 100;
                var helpBoxRect = new Rect(screenRect.center.x - helpBoxWidth * 0.5f, movingY, helpBoxWidth, helpBoxHeight);
                EditorGUI.HelpBox(helpBoxRect, "No categories found.", MessageType.Warning);
                return;
            }

            // Tab Bar
            float buttonWidth = 3 * TabBarButtonWidth;
            var tabBarRect = new Rect(screenRect.center.x - buttonWidth * 0.5f, movingY, buttonWidth, TabBarHeight);
            DrawTabBar(tabBarRect);

            movingY += tabBarRect.height + ContentToTabBarDistance;

            // Hierarchy
            float contentHeight = screenRect.height - movingY;
            var hierarchyRect = new Rect(movingX, movingY, hierarchyWidth, contentHeight);
            DrawHierarchyView(hierarchyRect);

            // Separator
            movingX += hierarchyWidth + EdgePadding;
            var separatorRect = new Rect(movingX, movingY, SeparatorWidth, contentHeight);
            DrawSeparator(separatorRect);

            // Inspector
            movingX += separatorRect.width + EdgePadding;
            var inspectorRect = new Rect(movingX, movingY, screenRect.width - movingX, contentHeight);
            DrawInspector(inspectorRect);
        }

        private void DrawToolBar(Rect viewRect)
        {
            float movingX = viewRect.x;
            const float newButtonWidth = 40;
            const float saveButtonWidth = 45;
            const float genButtonWidth = 70;
            const float configButtonWidth = 48;
            const float nameFieldWidth = 100;
            const float minModuleFilePickerWidth = 50;
            const float padding = EdgePadding;

            var newButtonRect = new Rect(movingX, viewRect.y, newButtonWidth, viewRect.height);
            createNewModuleFileSwitch.Set(GUI.Button(newButtonRect, "New"));
            movingX += newButtonWidth + padding;

            var saveButtonRect = new Rect(movingX, viewRect.y, saveButtonWidth, viewRect.height);
            saveModuleFileSwitch.Set(GUI.Button(saveButtonRect, "Save"));
            movingX += saveButtonWidth + padding;

            var genButtonRect = new Rect(movingX, viewRect.y, genButtonWidth, viewRect.height);
            generateFilesSwitch.Set(GUI.Button(genButtonRect, "Generate"));
            movingX += genButtonWidth + padding;

            var configButtonRect = new Rect(movingX, viewRect.y, configButtonWidth, viewRect.height);
            openConfigSwitch.Set(GUI.Button(configButtonRect, "Config"));
            movingX += configButtonWidth + padding;

            var nameFieldRect = new Rect(movingX, viewRect.y, nameFieldWidth, viewRect.height);
            string tmpName = GUI.TextField(nameFieldRect, moduleName);
            if (moduleName != tmpName)
            {
                moduleName = tmpName;
                nameModifiedSwitch.Activate();
            }
            movingX += nameFieldWidth + padding;

            var moduleFileRect = new Rect(movingX, viewRect.y, Mathf.Max(minModuleFilePickerWidth, viewRect.width - movingX), viewRect.height);
            var tmpModuleFile = (TextAsset) EditorGUI.ObjectField(moduleFileRect, moduleFile, typeof (TextAsset), false);

            if (tmpModuleFile != moduleFile)
            {
                // Class module file changed
                pendingModuleFile = tmpModuleFile;
            }
        }

        private void DrawTabBar(Rect viewRect)
        {
            Rect buttonRect = viewRect;
            buttonRect.xMax = viewRect.x + TabBarButtonWidth;

            ICodeGenerationCategory[] categories = CodeGenerationHelper.GetCodeGenerationCategories();

            for (var i = 0; i < categories.Length; i++)
            {
                DrawTabButton(categories[i], buttonRect);
                buttonRect.x += TabBarButtonWidth;
            }
        }

        private void DrawTabButton(ICodeGenerationCategory cat, Rect rect)
        {
            bool currentValue = category == cat;
            bool tmpValue = GUI.Toggle(rect, currentValue, cat.Name, GUI.skin.button);

            if (tmpValue != currentValue)
            {
                pendingCategory = cat;
            }
        }

        private void DrawHierarchyView(Rect viewRect)
        {
            float rowHeight = EditorGUIUtility.singleLineHeight;
            const float rowHeightPadding = 1;
            float toolBarHeight = EditorGUIUtility.singleLineHeight;
            const float buttonWidth = 20;

            // Calculate content size
            float movingY = viewRect.y;
            float movingX = viewRect.x;

            var addButtonRect = new Rect(movingX, movingY, buttonWidth, toolBarHeight);
            addDataSwitch.Set(GUI.Button(addButtonRect, "+"));
            movingX += buttonWidth + EdgePadding;

            var delButtonRect = new Rect(movingX, movingY, buttonWidth, toolBarHeight);
            delDataSwitch.Set(GUI.Button(delButtonRect, "-"));
            movingX = viewRect.x;
            movingY += toolBarHeight + EdgePadding;

            var scrollRect = new Rect(movingX, movingY, viewRect.width, viewRect.height - (movingY - viewRect.y));
            Rect contentRect = scrollRect;
            hierarchyScrollPosition = GUI.BeginScrollView(scrollRect, hierarchyScrollPosition, contentRect);
            {
                DataDraft[] datas = CodeGenerationHelper.GetDataDraftsForCategory(category, module);

                Rect labelRect = contentRect;
                labelRect.y += rowHeightPadding;
                labelRect.height = rowHeight;

                for (var i = 0; i < datas.Length; i++)
                {
                    DataDraft dataDraft = datas[i];

                    bool toggleActive = selectedDataDraft == dataDraft;

                    if (toggleActive)
                    {
                        // Highlight
                        if (Event.current.type == EventType.Repaint)
                        {
                            SelectedLabelStyle.Draw(labelRect, false, false, false, false);
                        }
                    }

                    bool toggleResult = GUI.Toggle(labelRect, toggleActive, dataDraft.Name, GUI.skin.label);
                    if (toggleResult != toggleActive && toggleActive == false)
                    {
                        pendingDataDraftSelection = dataDraft;
                    }


                    labelRect.y += rowHeight + rowHeightPadding * 2;
                }
            }
            GUI.EndScrollView();
        }

        private void DrawSeparator(Rect viewRect)
        {
            GUI.Box(viewRect, GUIContent.none, boxStyle);
        }

        private void DrawInspector(Rect viewRect)
        {
            if (selectedDataDraft == null)
            {
                return;
            }

            const float rowHeight = 17;
            const float rowHeightPadding = 2;

            Rect contentRect = viewRect;

            inspectorScrollPosition = GUI.BeginScrollView(viewRect, inspectorScrollPosition, contentRect);
            {
                Rect textRect = contentRect;
                textRect.y += rowHeightPadding;
                textRect.height = rowHeight;
                DataDraft dataDraft = selectedDataDraft;

                // Display Name
                dataDraft.Name = GUI.TextField(textRect, dataDraft.Name);

                // Display Reorderable List of Actions
                Rect listRect = contentRect;
                listRect.yMin += rowHeightPadding * 2 + textRect.height;

                if (categoryEditor != null)
                {
                    categoryEditor.DrawInspector(listRect);
                }
            }
            GUI.EndScrollView();
        }

        [MenuItem("Shygen/Generation Editor")]
        [UsedImplicitly]
        public static void GetWindow()
        {
            GetWindow<ModuleEditorWindow>(false, "Generation Editor", true);
        }
    }
}