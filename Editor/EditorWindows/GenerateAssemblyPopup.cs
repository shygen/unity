#region Disclaimer
// <copyright file="GenerateAssemblyPopup.cs">
// Copyright (c) 2019 - 2019 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion

using System.Linq;

namespace Shygen.Unity.Editor.EditorWindows
{
    using System;
    using System.Collections.Generic;
    using RobinBird.Utilities.Runtime.Extensions;
    using RobinBird.Utilities.Unity.Editor.Helper;
    using Shygen.Editor;
    using UnityEditor;
    using UnityEngine;

    public class GenerateAssemblyPopup : EditorWindow
    {
        private Dictionary<string, CodeGenerationHelper.GroupGeneration> groups;
        private GuiSwitch<string[]> generateSwitch;
        private Vector2 scrollRectPosition;
        private GUIStyle darkBox;
        

        private void OnEnable()
        {
            CodeGenerationEditorSettings.UseRoslynChanged += Repaint;
        }

        private void OnDisable()
        {
            CodeGenerationEditorSettings.UseRoslynChanged -= Repaint;
        }

        private Dictionary<string, CodeGenerationHelper.GroupGeneration> Groups
        {
            get
            {
                if (groups == null)
                {
                    groups = new Dictionary<string, CodeGenerationHelper.GroupGeneration>();
                }

                return groups;
            }
        }

        private static void ExtractGroups(Dictionary<string, CodeGenerationHelper.GroupGeneration> groups)
        {
            var allGroups = new Dictionary<string, CodeGenerationHelper.GroupGeneration>();
            if (CodeGenerationEditorSettings.Instance.UseRoslyn)
            {
                CodeGenerationHelper.ExtractRoslynGroupConfigs(allGroups);
            }
            else
            {
                var types = AppDomain.CurrentDomain.GetTypes(typeof(object));

                foreach (var type in types)
                {
                    CodeGenerationHelper.ExtractGroupConfigs(type, allGroups);
                }
            }

            // We have multiple groups with the same name but for different scopes
            foreach (KeyValuePair<string,CodeGenerationHelper.GroupGeneration> pair in allGroups)
            {
                if (groups.Any(valuePair => valuePair.Value.Name == pair.Value.Name) == false)
                {
                    groups.Add(pair.Value.Name, pair.Value);
                }
            }
        }

        private void Update()
        {
            if (generateSwitch.Use())
            {
                GenerateFromAssembly(generateSwitch.Data);
            }
        }

        private void OnGUI()
        {
            if (darkBox == null)
            {
                darkBox = new GUIStyle(GUI.skin.box) {normal = {background = Texture2D.grayTexture}};
            }
            bool useRoslyn = CodeGenerationEditorSettings.Instance.UseRoslyn;
            bool tempUseRoslyn = EditorGUILayout.Toggle(
                new GUIContent("Use Roslyn",
                    "When enabled Shygen can gather information about the Modules without needing a compiling version of the code"),
                useRoslyn);

            if (tempUseRoslyn != useRoslyn)
            {
                CodeGenerationEditorSettings.Instance.UseRoslyn = tempUseRoslyn;
            }
            
            EditorGUI.BeginDisabledGroup(useRoslyn == false);
            {
                string dotnetPath = CodeGenerationEditorSettings.Instance.DotnetPath;

                var tempDotnetPath = EditorGUILayout.TextField("Dotnet Path", dotnetPath);
                if (tempDotnetPath != dotnetPath)
                {
                    CodeGenerationEditorSettings.Instance.DotnetPath = tempDotnetPath;
                }
            }
            EditorGUI.EndDisabledGroup();



            if (GUILayout.Button("Refresh"))
            {
                ExtractGroups(groups);
            }
            
            if (GUILayout.Button("All") && EditorUtility.DisplayDialog("Generate all", "Do you really want to regenerate all? Takes some minutes.", "Yep", "No"))
            {
                GenerateFromAssembly(null);
            }

            scrollRectPosition = EditorGUILayout.BeginScrollView(scrollRectPosition);
            bool isDark = false;
            foreach (KeyValuePair<string,CodeGenerationHelper.GroupGeneration> pair in Groups)
            {
                var group = pair.Value;
                EditorGUILayout.BeginVertical(GUI.skin.box);
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        EditorGUILayout.LabelField("Group: " + group.Name, EditorStyles.boldLabel);
                        GUILayout.FlexibleSpace();
                        if (GUILayout.Button("Generate Group"))
                        {
                            generateSwitch.Activate(group.ModuleNames);
                        }
                    }
                    EditorGUILayout.EndHorizontal();

                    foreach (string moduleName in group.ModuleNames)
                    {
                        EditorGUILayout.BeginHorizontal(isDark ? darkBox : GUI.skin.box);
                        {
                            EditorGUILayout.LabelField(moduleName);
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button("Generate"))
                            {
                                generateSwitch.Activate(new [] { moduleName });
                            }
                        }
                        EditorGUILayout.EndHorizontal();
                        isDark = !isDark;
                    }
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndScrollView();
            
        }

        private void GenerateFromAssembly(string[] modules)
        {
            EditorUtility.DisplayProgressBar("Generate Assembly", "Generating...", 0);
            try
            {
                CodeGenerationHelper.GenerateFromCurrentAppDomain(EditorTools.GetProjectPath(), modules);
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }

            CodeGenerationEditorSettings.CodeGenerationFinished(modules);
            AssetDatabase.Refresh();
        }
    }
}