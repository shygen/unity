﻿#region Disclaimer

// <copyright file="CodeGenerationSettingsEditorWindow.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace Shygen.Unity.Editor.EditorWindows
{
    using System.Collections.Generic;
    using System.IO;
    using JetBrains.Annotations;
    using RobinBird.Utilities.Unity.Editor.Helper;
    using Shygen.Editor;
    using Shygen.Editor.Interfaces;
    using UnityEditor;
    using UnityEditorInternal;
    using UnityEngine;

    /// <summary>
    /// Editor window to configure which modules should be generated and where
    /// there respected module files are stored.
    /// Also configure which templates to use while generating
    /// </summary>
    public class CodeGenerationSettingsEditorWindow : EditorWindow
    {
        private CodeGenerationSettings settings;

        private List<ReorderableList> categoryLists;

        /// <summary>
        /// Reorderable lists to display the module paths inside the group.
        /// </summary>
        private List<ReorderableList> modulePathsLists;

        /// <summary>
        /// Reorderable list to display all groups.
        /// </summary>
        private ReorderableList moduleGroupSettingsList;

        private Vector2 scrollPosition;

        private GuiLever<EditorMode> modeLever;

        [UsedImplicitly]
        private void OnEnable()
        {
            // Load Settings
            settings = UnityCodeGenerationHelper.ReadEditorSettings();

            categoryLists = new List<ReorderableList>();

            modulePathsLists = new List<ReorderableList>();

            CreateLists();
        }

        private void CreateLists()
        {
            categoryLists.Clear();
            for (var i = 0; i < CodeGenerationHelper.GetCodeGenerationCategories().Length; i++)
            {
                ICodeGenerationCategory category = CodeGenerationHelper.GetCodeGenerationCategories()[i];

                CodeGenerationSettings.CodeGenerationCategorySetting categorySetting = settings.GetSettingsForCategory(category.Name);

                if (categorySetting == null)
                {
                    categorySetting = new CodeGenerationSettings.CodeGenerationCategorySetting();
                    categorySetting.CategoryName = category.Name;
                    settings.CategorySettings.Add(categorySetting);
                }

                var reorderableList = new ReorderableList(categorySetting.Settings, typeof (CodeGenerationSettings.CodeGenerationSetting));
                reorderableList.drawHeaderCallback += rect => EditorGUI.LabelField(rect, category.Name);
                reorderableList.elementHeight = EditorGUIUtility.singleLineHeight;
                reorderableList.drawElementCallback += (rect, index, active, focused) =>
                {
                    CodeGenerationSettings.CodeGenerationSetting setting = categorySetting.Settings[index];
                    var destination = setting.Path;
                    destination.Path = EditorTools.FileField(rect, GUIContent.none, setting.Path.Path, true, ".", "tat,tat.cs");
                    setting.Path = destination;
                };
                reorderableList.onAddCallback += list => { categorySetting.Settings.Add(new CodeGenerationSettings.CodeGenerationSetting()); };
                reorderableList.onRemoveCallback += list => { categorySetting.Settings.RemoveAt(list.index); };
                reorderableList.onCanRemoveCallback += list => true;

                categoryLists.Add(reorderableList);
            }

            for (var i = 0; i < settings.ModuleGroupSettings.Count; i++)
            {
                CodeGenerationSettings.ModuleGroupSetting moduleGroup = settings.ModuleGroupSettings[i];

                modulePathsLists.Add(CreateReorderableListForModuleGroupSetting(moduleGroup));
            }

            moduleGroupSettingsList = new ReorderableList(settings.ModuleGroupSettings, typeof (CodeGenerationSettings.ModuleGroupSetting));
            moduleGroupSettingsList.drawHeaderCallback += rect => EditorGUI.LabelField(rect, "Module Setting Groups");
            moduleGroupSettingsList.elementHeightCallback += index =>
            {
                float height = EditorGUIUtility.singleLineHeight * 3;
                height += 3; // padding
                ReorderableList groupSettingList = modulePathsLists[index];
                height += groupSettingList.GetHeight();
                return height;
            };
            moduleGroupSettingsList.drawElementCallback += (rect, index, active, focused) =>
            {
                CodeGenerationSettings.ModuleGroupSetting group = settings.ModuleGroupSettings[index];

                // Name
                Rect firstRect = rect;
                firstRect.height = EditorGUIUtility.singleLineHeight;
                group.Name = EditorGUI.TextField(firstRect, "Name", group.Name);

                // Namespace
                Rect namespaceRect = firstRect;
                namespaceRect.y += EditorGUIUtility.singleLineHeight;
                group.Namespace = EditorGUI.TextField(namespaceRect, "Namespace", group.Namespace);

                // Generation Target
                Rect secondRect = namespaceRect;
                secondRect.y += EditorGUIUtility.singleLineHeight;
                group.TargetDirectoryPath = EditorTools.DirectoryPathField(secondRect, "Target Directory", group.TargetDirectoryPath, true);

                // Module Files
                ReorderableList moduleFileReorderableList = modulePathsLists[index];

                Rect restRect = secondRect;
                restRect.y += EditorGUIUtility.singleLineHeight;
                restRect.height = rect.height - secondRect.height - namespaceRect.height - firstRect.height - EditorGUIUtility.singleLineHeight;
                moduleFileReorderableList.DoList(restRect);
            };
            moduleGroupSettingsList.onAddCallback += list =>
            {
                var group = new CodeGenerationSettings.ModuleGroupSetting();
                settings.ModuleGroupSettings.Add(group);
                modulePathsLists.Add(CreateReorderableListForModuleGroupSetting(group));
            };
            moduleGroupSettingsList.onRemoveCallback += list =>
            {
                settings.ModuleGroupSettings.RemoveAt(list.index);
                modulePathsLists.RemoveAt(list.index);
            };
            moduleGroupSettingsList.onCanRemoveCallback += list => true;
        }

        private ReorderableList CreateReorderableListForModuleGroupSetting(CodeGenerationSettings.ModuleGroupSetting group)
        {
            var list = new ReorderableList(group.ModulePaths, typeof (string));

            list.drawHeaderCallback += rect => EditorGUI.LabelField(rect, "Module Paths");
            list.elementHeight = EditorGUIUtility.singleLineHeight;
            list.onAddCallback += reorderableList => { group.ModulePaths.Add(string.Empty); };
            list.onRemoveCallback += reorderableList => { group.ModulePaths.RemoveAt(reorderableList.index); };
            list.drawElementCallback +=
                (rect, index, active, focused) => { group.ModulePaths[index] = EditorTools.FileField(rect, GUIContent.none, group.ModulePaths[index], true, ".", "json"); };

            return list;
        }

        [UsedImplicitly]
        // ReSharper disable once InconsistentNaming
        private void OnGUI()
        {
            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
            {
                if (GUILayout.Button("Save"))
                {
                    Save();
                }
                if (GUILayout.Button("Regenerate All"))
                {
                    GenerateAll();
                }
                if (GUILayout.Button("Generate From Assembly"))
                {
                    GenerateFromAssembly();
                }

                DrawTabNavigation();

                switch (modeLever.Active)
                {
                    case EditorMode.ModuleGroups:
                        DrawModuleGroups();
                        break;
                    case EditorMode.Categories:
                        DrawCategories();
                        break;
                }
            }
            EditorGUILayout.EndScrollView();
        }


        private void DrawTabNavigation()
        {
            const int tabWidth = 100;
            const int tabHeight = 40;

            EditorGUILayout.BeginHorizontal();
            {
                GUILayout.FlexibleSpace();
                bool moduleActive = modeLever.Active == EditorMode.ModuleGroups;
                if (GUILayout.Toggle(moduleActive, "Module Groups", GUI.skin.button, GUILayout.Width(tabWidth), GUILayout.Height(tabHeight)) && moduleActive == false)
                {
                    modeLever.Pending = EditorMode.ModuleGroups;
                }
                bool templateActive = modeLever.Active == EditorMode.Categories;
                if (GUILayout.Toggle(templateActive, "Categories", GUI.skin.button, GUILayout.Width(tabWidth), GUILayout.Height(tabHeight)) && templateActive == false)
                {
                    modeLever.Pending = EditorMode.Categories;
                }
                GUILayout.FlexibleSpace();
            }
            EditorGUILayout.EndHorizontal();
        }

        private void DrawModuleGroups()
        {
            moduleGroupSettingsList.DoLayoutList();
        }

        private void DrawCategories()
        {
            for (var i = 0; i < CodeGenerationHelper.GetCodeGenerationCategories().Length; i++)
            {
                ReorderableList list = categoryLists[i];
                list.DoLayoutList();
            }
        }

        private void Save()
        {
            UnityCodeGenerationHelper.WriteEditorSettings(settings);

            ShowNotification(new GUIContent("Saved"));
        }

        [MenuItem("Shygen/Regenerate All Json")]
        public static void GenerateAll()
        {
            Debug.Log("Start generate all");
            var settings = UnityCodeGenerationHelper.ReadEditorSettings();
            var jobCount = 0;
            for (var i = 0; i < settings.ModuleGroupSettings.Count; i++)
            {
                CodeGenerationSettings.ModuleGroupSetting group = settings.ModuleGroupSettings[i];
                for (var j = 0; j < group.ModulePaths.Count; j++)
                {
                    jobCount++;
                }
            }

            float part = 1f / jobCount;
            float progress = 0;
            for (var i = 0; i < settings.ModuleGroupSettings.Count; i++)
            {
                CodeGenerationSettings.ModuleGroupSetting group = settings.ModuleGroupSettings[i];

                for (var j = 0; j < group.ModulePaths.Count; j++)
                {
                    string modulePath = group.ModulePaths[j];
                    Debug.Log(string.Format("Generating {0} Group - {1} Module", group.Name, Path.GetFileNameWithoutExtension(modulePath)));
                    EditorUtility.DisplayProgressBar("Generating", string.Format("{0} Group - {1} Module", group.Name, Path.GetFileNameWithoutExtension(modulePath)), progress);
                    UnityCodeGenerationHelper.GenerateFiles(settings, modulePath, group);
                    progress += part;
                }
            }
            EditorUtility.ClearProgressBar();
            Debug.Log("Finish generate all");
        }

        [MenuItem("Shygen/Regenerate All Assembly")]
        public static void GenerateFromAssembly()
        {
            EditorUtility.DisplayProgressBar("Generate Assembly", "Generating...", 0);
            CodeGenerationHelper.GenerateFromCurrentAppDomain(EditorTools.GetProjectPath());
            EditorUtility.ClearProgressBar();
            AssetDatabase.Refresh();
        }
        
        [MenuItem("Shygen/Generate...")]
        public static void ContextTest()
        {
            GetWindow<GenerateAssemblyPopup>(false, "Generate");
        }

        [UsedImplicitly]
        private void Update()
        {
            if (modeLever.TrySwitch())
            {
                // Switched
            }
        }

        [MenuItem("Edit/Project Settings/Code Generation")]
        public static void GetWindow()
        {
            GetWindow<CodeGenerationSettingsEditorWindow>(false, "Gen. Settings", true);
        }

        private enum EditorMode
        {
            ModuleGroups,
            Categories,
        }
    }
}