using System;
using System.IO;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Unity.Editor.Helper;
using Shygen.Editor;
using UnityEditor;

namespace Shygen.Unity.Editor
{
    /// <summary>
    /// Settings which control how the code is being generated
    /// </summary>
    public class CodeGenerationEditorSettings : ICodeGenerationEditorSettings
    {
        private const string ShygenEditorReadmeGuid = "9c6709a3fb59d41bfb1a01f61ef4ece5";
        
        public const string UseRoslynName = "Shygen.CodeGenerationEditorSettings.UseRoslyn";
        public const string DotnetPathName = "Shygen.CodeGenerationEditorSettings.DotnetPath";
        

        private static CodeGenerationEditorSettings instance;
        
        public static CodeGenerationEditorSettings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CodeGenerationEditorSettings();
                }

                return instance;
            }
        }

        public static Action UseRoslynChanged;

        public static Action<string[]> CodeGenerationFinished;
        
        /// <summary>
        /// Control if Roslyn should be used to gather information about the types to generate. Making this a setting
        /// because not all machines can execute the Roslyn process or have the lib installed. If this is disabled you
        /// have to always have correctly compiling code
        /// </summary>
        public bool UseRoslyn
        {
            get { return EditorPrefs.GetBool(UseRoslynName, true); }
            set { EditorPrefs.SetBool(UseRoslynName, value); }
        }

        public string AnalyzerPath
        {
            get
            {
                var editorReadmePath = new FileInfo(Path.Combine(EditorTools.GetProjectPath(), AssetDatabase.GUIDToAssetPath(ShygenEditorReadmeGuid)));
                if (editorReadmePath.Exists == false)
                {
                    Log.Error($"Could not find editor readme path which is used to find Roslyn Analyzer. Tried: {editorReadmePath.FullName}");
                    return string.Empty;
                }
                var editorDirectory = editorReadmePath.Directory;
                if (editorDirectory.Exists == false)
                {
                    Log.Error($"Could not find analyzer directory. Tried: {editorDirectory.FullName}");
                    return string.Empty;
                }
                var anaylzerFile = new FileInfo(Path.Combine(editorDirectory.FullName,
                    "Roslyn~/RoslynSyntaxWalker/final/netcoreapp3.1/RoslynSyntaxWalker.dll"));
                if (anaylzerFile.Exists == false)
                {
                    Log.Error($"Could not find analyzer file. Tried: {anaylzerFile.FullName}");
                    return string.Empty;
                }
                return EditorPrefs.GetString(UseRoslynName, anaylzerFile.FullName);
            }
            set { EditorPrefs.SetString(UseRoslynName, value); }
        }
        
        public string DotnetPath
        {
            get { return EditorPrefs.GetString(DotnetPathName, "/usr/local/share/dotnet/dotnet"); }
            set { EditorPrefs.SetString(DotnetPathName, value); }
        }


        [MenuItem("Shygen/Use Roslyn", false)]
        public static bool SwitchUseRoslyn()
        {
            Instance.UseRoslyn = !Instance.UseRoslyn;
            UseRoslynChanged?.Invoke();
            return Instance.UseRoslyn;
        }
        
        [MenuItem("Shygen/Use Roslyn", true)]
        public static bool ValidateUseRoslyn()
        {
            Menu.SetChecked("Shygen/Use Roslyn", Instance.UseRoslyn);
            return true;
        }
    }
}