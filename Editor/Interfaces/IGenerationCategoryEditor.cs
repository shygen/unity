﻿#region Disclaimer
// <copyright file="IGenerationCategoryEditor.cs">
// Copyright (c) 2017 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion
namespace Shygen.Unity.Editor.Interfaces
{
    using Shygen.Editor;
    using Shygen.Editor.Interfaces;
    using UnityEngine;

    public interface IGenerationCategoryEditor
    {
        void Initialize(object data);

        void DrawInspector(Rect rect);

        DataDraft SelectedDataDraft { get; set; }
    }
    
    public abstract class GenerationCategoryEditor<T> : IGenerationCategoryEditor where T : ICodeGenerationCategory
    {
        protected virtual void Initialize(T data)
        {
            
        }

        public void Initialize(object data)
        {
            Initialize((T)data);
        }

        public abstract void DrawInspector(Rect rect);

        public abstract DataDraft SelectedDataDraft { get; set; }
    }
}